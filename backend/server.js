const express = require('express')
const cors = require('cors')
const app = express()

const { syncDataFiles } = require('./sync_data.js')
const { getDurationData } = require('./handle_queries.js')

const port = process.env.PORT || 5000
let data = {} //data is used as cache so the server won't read the json files every time it gets a request
const syncEveryHour = 1

//sync coin data every hour
setInterval(async () => {
    console.log("syncing data")
    await syncDataFiles(data)
}, syncEveryHour * 60 * 60 * 1000) //converting hours to miliseconds

app.use(cors())

app.get("/", async (req, res) => {
    //if there are no queries, send the client the react files
    if (Object.keys(req.query).length === 0) {
        app.use(express.static(__dirname + "/build"))
        res.sendFile(__dirname + "/build/index.html")
    }
    //if there are queries, send the client the requested data about coin
    else {
        let coin = req.query.coin
        let duration = req.query.duration

        //if coin hasn't been saved, save coin in data
        if (!(coin in data)) {
            console.log(`Saving ${coin}`)
            data[coin] = {}
        }
        //if duration data hasn't been saved, save duration data
        if (!(duration in data[coin])) {
            console.log(`adding ${duration} to ${coin}`)
            data[coin][duration] = await getDurationData(coin, duration)
        }
        res.json(data[coin][duration]) //sending client requested data
    }
})

app.listen(port, () => {
    console.log(`Server started on port ${port}`)
})