#the scripts is used to fetch data about coins from coinApi and orgenize it in files.

import requests

COINS = ["BTC", "ETH", "DOGE", "ADA"]
YEARS = [year for year in range(2018, 2023)]
API_KEY = "711C2FD0-A348-4D16-99D7-922D1354A79B"

def main():
    for coin in COINS:
        for year in YEARS:
            with open(f"./crypto_data/{coin}-{year}.json", "w") as data_file:
                data = requests.get(f"https://rest.coinapi.io/v1/exchangerate/{coin}/USD/history?period_id=1DAY&time_start={year}-01-01T00:00:00&time_end={year}-12-31T00:00:00&limit=365&apikey={API_KEY}")
                data_file.write(
                    data.text
                )

if __name__ == "__main__":
    main()