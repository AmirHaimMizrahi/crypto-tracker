#the script is used to merge all the data files to one file.
#data files for every year will get merged into one coin data file.

import json

COINS = ["BTC", "ETH", "DOGE", "ADA"]
YEARS = [year for year in range(2018, 2023)]

def main():
    for coin in COINS:
        coin_data = []

        #reading all data
        for year in YEARS:
            with open(f'./crypto_data/coin-years/{coin}-{year}.json') as data_file:
                json_data = json.load(data_file)

                for day in json_data:
                    coin_data.append(day)
        #writing data to new file
        with open(f'./crypto_data/{coin}.json', 'w') as data_file:
            json.dump(coin_data, data_file, indent=3)

if __name__ == "__main__":
    main()