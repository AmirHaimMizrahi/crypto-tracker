#adding the average value of the coins to the json files.

import json

COINS = ["BTC", "ETH", "DOGE", "ADA"]
YEARS = [year for year in range(2018, 2023)]

def calc_avg(json_data, index):
    average_count = min(30, len(json_data) - index)
    average = 0
    for j in range(average_count):
        average += json_data[index + j]["rate_open"]

    json_data[index]["average"] = average / average_count

def main():
    for coin in COINS:
        json_data = []

        with open(f'./crypto_data/{coin}.json', 'r') as data_file:
            json_data = json.load(data_file)

            for i in range (len(json_data)):
                calc_avg(json_data, i)

        with open(f'./crypto_data/{coin}.json', 'w') as data_file:
            json.dump(json_data, data_file, indent=3)



if __name__ == "__main__":
    main()