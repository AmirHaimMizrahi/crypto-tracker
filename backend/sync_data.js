const { readFile, writeFile } = require('fs').promises
const { subDays } = require('date-fns')
const fetch = require('node-fetch')

/**
 * checking if two iso represent smae day
 * @param {iso formatted string} iso0 first iso string
 * @param {iso formatted string} iso1 second iso strings
 * @returns whether the two iso string represent the same date regardless the hours, minutes and seconds
 */
function isSameDay(iso0, iso1) {
    const date0 = new Date(iso0)
    const date1 = new Date(iso1)
    return (
        (date0.getFullYear() === date1.getFullYear()) &&
        (date0.getMonth() === date1.getMonth()) &&
        (date0.getDate() === date1.getDate())
    )
}

/**
 * asynchronouse parse json file
 * @param {string} path json file path to read and parse
 * @returns 
 */
async function readJsonFile(path) {
    return JSON.parse(await readFile(path, "utf8"))
}

/**
 * asynchronous syncing data about coins and adding the data to the json files
 * @param {object - coins data} data coins data
 */
async function syncDataFiles(data) {
    //removing "now" from data so the clients will fetch the new data
    for (const key in data) {
        delete data[key]["now"]
    }
    await updateJsonFile(`./crypto_data/BTC.json`, await getCoinData("BTC"));
    await updateJsonFile(`./crypto_data/ETH.json`, await getCoinData("ETH"));
    await updateJsonFile(`./crypto_data/DOGE.json`, await getCoinData("DOGE"));
    await updateJsonFile(`./crypto_data/ADA.json`, await getCoinData("ADA"));
}

/**
 * asynchronous fetching data about a coin using the coinApi
 * @param {string} coin coin symbol name
 * @returns new fetched data about coin
 */
async function getCoinData(coin) {
    const apiKey = "711C2FD0-A348-4D16-99D7-922D1354A79B"

    const response = await fetch(`https://rest.coinapi.io/v1/exchangerate/${coin}/USD/?apikey=${apiKey}`)
    const data = await response.json()

    //changing keys' names 
    data["time_open"] = data["time"]
    data["rate_open"] = data["rate"]
    delete data["time"]
    delete data["rate"]

    return data
}

function recalculateAverage(data) {
    //calc average 30 days to the future
    let i = 0;
    const iso30DaysAgo = subDays(new Date(), 30).toISOString()

    //calculating index of 30 days ago data
    for (i = data.length; i > 0; i--) {
        if (isSameDay(iso30DaysAgo, data.at(-i).time_open)) {
            break;
        }
    }

    //recalculating average for 30 days ago
    for (i; i > 0; i--) {
        let average = 0

        for (let j = 0; j < i; j++) {
            average += data.at(-i + j).rate_open
        }

        data.at(-i)["average"] = average / i
    }
}

/**
 * appending data to json file. after adding the data, recalulating the average of the last 30 values
 * @param {string} path path of json file to update
 * @param {object} data data to append to json file
 */
async function updateJsonFile(path, data) {
    let json = await readJsonFile(path)
    json.push(data) //adding data to file

    recalculateAverage(json)

    await writeFile(path, JSON.stringify(json, null, 3)) //writing changes to json file
}

module.exports.syncDataFiles = syncDataFiles
module.exports.readJsonFile = readJsonFile
module.exports.isSameDay = isSameDay