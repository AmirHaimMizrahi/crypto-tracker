const { subDays } = require('date-fns')
const { readJsonFile, isSameDay } = require('./sync_data.js')

/**
 * getting all data from local time to a certain amount of time back
 * @param {number} daysAgo amount of days ago
 * @param {array of data about a coin} data base data to extract duration data from
 * @returns all the data from local time to daysAgo parameter
 */
function getXDaysAgoData(daysAgo, data) {
    const xDaysAgoISO = subDays(new Date(), daysAgo).toISOString() //getting iso representation of daysAgo

    //looking for daysAgo is data
    for (let i = data.length - 1; i >= 0; i--) {
        if (isSameDay(data[i].time_open, xDaysAgoISO)) {
            return data.slice(i)
        }
    }
    //if daysAgo not found, return whole data
    return data
}

/**
 * getting coin data for a certain duration back 
 * @param {string} coin coin symbol
 * @param {string} duration string representing the data duration to fetch. can be: 5Y, 1Y, 3M, 1M, 7d, now
 * @returns duration data
 */
async function getDurationData(coin, duration) {
    let jsonData = []

    switch (duration) {
        //for every duration, reading json file and fetching the correct amount of time back
        case "5Y":
            jsonData = await readJsonFile(`./crypto_data/${coin}.json`)
            return getXDaysAgoData(365 * 5 + 1, jsonData) //+ 1 for at least 1 leap year

        case "1Y":
            jsonData = await readJsonFile(`./crypto_data/${coin}.json`)
            return getXDaysAgoData(365, jsonData)

        case "3M":
            jsonData = await readJsonFile(`./crypto_data/${coin}.json`)
            return getXDaysAgoData(90, jsonData)

        case "1M":
            jsonData = await readJsonFile(`./crypto_data/${coin}.json`)
            return getXDaysAgoData(30, jsonData)

        case "7d":
            jsonData = await readJsonFile(`./crypto_data/${coin}.json`)
            return getXDaysAgoData(7, jsonData)

        case "now":
            jsonData = await readJsonFile(`./crypto_data/${coin}.json`)
            return jsonData.at(-1)
    }
}

module.exports.getDurationData = getDurationData