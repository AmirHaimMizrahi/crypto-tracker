import React, { useContext } from 'react'
import CoinBar from '../components/CoinBar'
import './HomePage.css'
import { ThemeContext } from '../theme'
import Switch from '../components/Switch'

/**
 * the HomePage component will render all the coin bars, titles and theme toggle switch
 */
export default function HomePage() {
    const { theme, toggleTheme } = useContext(ThemeContext)

    return (
        <div className="homepage">
            <h1 className="title" >Crypto Tracker</h1>
            <h4 className="copyright">Made by Amir Haim Mizrahi</h4>
            <h5 className="gitlabLink"><a href="https://gitlab.com/AmirHaimMizrahi/crypto-tracker">to Gitlab Page</a></h5>

            <div className="themeToggler">
                <span className={theme === "dark" ? "bold" : "notBold"}>Dark</span>
                <Switch onToggle={toggleTheme} />
                <span className={theme === "light" ? "bold" : "notBold"}>light</span>
            </div>
            <CoinBar name="Bitcoin" coin="BTC" />
            <CoinBar name="Etherium" coin="ETH" />
            <CoinBar name="Dodge Coin" coin="DOGE" />
            <CoinBar name="Cardano" coin="ADA" />
        </div>
    )
}
