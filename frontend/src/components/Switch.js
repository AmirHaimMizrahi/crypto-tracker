import { useState } from 'react'
import './Switch.css'

//a switch like checkbox.
//call the onToggle function every time the switch is toggled
export default function Switch({ onToggle }) {
    const [isToggled, setToggled] = useState(true)

    function toggle() {
        setToggled(prev => !prev)
    }

    return (
        <label className="switch">
            <input type="checkbox" onChange={onToggle}></input>
            <span className="slider"></span>
        </label>
    )
}
