import Graph from './Graph'

//The SmallCoinGraph will show a small 1 year graph that shows the value and average value of a coin. 
export default function SmallCoinGraph({ coin, height }) {
    return (
        <Graph coin={coin} duration="1Y" height={height} />
    )
}