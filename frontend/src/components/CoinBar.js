import React, { useState, useRef, useEffect } from 'react'
import SmallCoinGraph from './SmallCoinGraph'
import CoinGraph from './CoinGraph'
import './CoinBar.css'

//The CoinBar needs to render data about certain coins.
//The coin bar will show the coin's name and symbol, a 1 year graph and the current value of the coin.
//If expanded, the bar will show a more informative graph that allows to view the coin's value for different durations.
export default function CoinBar({ name, coin }) {
    const [isExpand, setExpand] = useState(false);
    const bodyRef = useRef();
    //body style for when isExpanded or not
    const bodyStyle = {
        height: isExpand ? bodyRef.current.scrollHeight : 0
    }
    const [currentCoinValue, setCurrentCoinValue] = useState("loading...")

    useEffect(() => { fetchCurrentCoinValue() }, []) //fetching current value every time the page is refreshed

    //fetching the current value of the coin
    async function fetchCurrentCoinValue() {
        const response = await fetch(`https://crypto-tracker-site.herokuapp.com/?coin=${coin}&duration=now`)
        const json = await response.json()

        setCurrentCoinValue("$" + json.rate_open.toFixed(2))
    }

    return (
        <div className="coinBar">
            <button className="head" onClick={() => { setExpand(!isExpand) }}>
                <div className="titles">
                    <h1 className="value">{coin}</h1>
                    <h3 className="text">{name}</h3>
                </div>
                <div className="currentValue">
                    <h1 className="value">{currentCoinValue}</h1>
                    <h3 className="text">current value</h3>
                </div>
                <div className="graph"><SmallCoinGraph height={100} coin={coin} /></div>
            </button>
            <div className="body" ref={bodyRef} style={bodyStyle}><CoinGraph height={400} coin={coin} /> </div>
        </div>
    )
}
