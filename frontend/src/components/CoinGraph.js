import React, { useState } from 'react'
import Graph from './Graph'
import { XAxis, YAxis, Tooltip, CartesianGrid } from 'recharts';
import './CoinGraph.css'

//The CoinGraph will show an informative graph the shows the value and average value of a coin.
//The graph contains buttons that allow to change the duration of which the graph will show information about.
export default function CoinGraph({ coin, height }) {
    const [duration, setDuration] = useState("5Y") //default duration is 5 years

    return (
        <div>
            <div className="durations">
                <button className={duration === "7d" ? "button clicked" : "button"} onClick={() => { setDuration("7d"); }}>7d</button>
                <button className={duration === "1M" ? "button clicked" : "button"} onClick={() => { setDuration("1M"); }}>1M</button>
                <button className={duration === "3M" ? "button clicked" : "button"} onClick={() => { setDuration("3M"); }}>3M</button>
                <button className={duration === "1Y" ? "button clicked" : "button"} onClick={() => { setDuration("1Y"); }}>1Y</button>
                <button className={duration === "5Y" ? "button clicked" : "button"} onClick={() => { setDuration("5Y"); }}>5Y</button>
            </div>
            <Graph coin={coin} duration={duration} height={height}>
                <XAxis
                    dataKey="date"
                />
                <YAxis
                    dataKey="value"
                    axisLine={false}
                    tickLine={false}
                    tickCount={6}
                    tickFormatter={(value) => `$${value}`}
                />
                <Tooltip formatter={(value, name, props) => value.toFixed(2)} />
                <CartesianGrid opacity={0.5} vertical={false} />
            </Graph>
        </div >
    )
}