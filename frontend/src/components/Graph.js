import { useState, useEffect, useContext } from 'react'
import { Legend, Line, LineChart, Area, AreaChart, ResponsiveContainer } from 'recharts';
import { format, parseISO, } from 'date-fns';
import ThemeContext from '../theme'

//Graph will show a graph about a certain coin.
//The graph will show the coin's value and the average value of the coin.
//The graph can show data for different durations.
//The graph can have different children so it can be modified
export default function Graph({ coin, duration, height, children }) {
  const [data, setData] = useState([])
  const { theme } = useContext(ThemeContext)

  useEffect(() => { fetchData() }, [duration]) //fetching new data about coin every time the duration changed

  //fetching coin data for a certain duration
  async function fetchData() {
    const response = await fetch(`/?coin=${coin}&duration=${duration}`)
    const json = await response.json()

    setData(json.map(day => ({ date: format(parseISO(day.time_open), "MMM d yyyy"), value: day.rate_open, average: day.average })))
  }

  return (
    <div>
      <ResponsiveContainer width="100%" height={height}>
        <AreaChart data={data}>
          <defs>
            <linearGradient id="valueColor" x1="0" y1="0" x2="0" y2="1">
              <stop offset="0%" stopColor="#ff0f0f" stopOpacity={0.7} />
              <stop offset="100%" stopColor={`var(--${theme}-background2)`} stopOpacity={0} />
            </linearGradient>

          </defs>
          <Legend verticalAlign="top" height={36} iconSize={20} />
          <Area dataKey="value" stroke="#992222" strokeWidth={1.5} fill="url(#valueColor)" />
          <Area dataKey="average" stroke="#229922" strokeWidth={2} fill="none" />
          {children}
        </AreaChart>
      </ResponsiveContainer>
    </div>
  )
}