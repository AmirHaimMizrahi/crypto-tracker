import { useState } from 'react';
import HomePage from '../pages/HomePage';
import ThemeContext from '../theme'

export default function App() {
  const [theme, setTheme] = useState("dark")

  //toggling the theme. if light, sets dark. if dark, sets light
  function toggleTheme() {
    setTheme(currentTheme => currentTheme === "dark" ? "light" : "dark")
  }

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <div id={theme}>
        <HomePage />
      </div>
    </ThemeContext.Provider >
  )
}
