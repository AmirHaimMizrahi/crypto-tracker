# Crypto Tracker



## About

A Node js and React web application to track Bitcoin, Etherium, Cardano and Dodge Coin.

The app can show the current value of the coins (updates every hour), and graphs that show the value and average value of the coins.

The graphs can change the duration they show to 5 years, 1 year, 3 months, 1 month and 7 days.

## How to run

To run the app, run the following commands:
```
git clone https://gitlab.com/AmirHaimMizrahi/crypto-tracker
cd ./crypto_tracker/backend
node server.js
```
To view the app [Click Here](http://localhost:5000)

